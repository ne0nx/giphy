# giphy

Проект можно запустить из 
- VScode
- Android Studio
для этого нужно установить язык Dart и Flatter.


Используется архитектура Redux.
Пакеты:

//- Кеширование и загрузка изображений
cached_network_image:

//- Redux
redux: ^3.0.0
flutter_redux: ^0.5.3

//- Http модуль
http: ^0.12.0+1