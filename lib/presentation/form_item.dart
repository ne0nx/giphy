
import 'package:flutter/material.dart';

typedef OnSearchCallback = Function(String search);

class FormItem extends StatelessWidget {
  final OnSearchCallback onSearch;

  FormItem({@required this.onSearch});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return TextField(
        style: textTheme.headline,
        decoration: InputDecoration(
          hintText: "Search images"
        ),
        onSubmitted: (text) {
          onSearch(text);
        },
      );
  }

}
