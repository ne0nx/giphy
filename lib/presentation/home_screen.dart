import 'package:flutter/material.dart';
import 'package:giphy/containers/loaded_images.dart';
import 'package:giphy/presentation/form_item.dart';

typedef OnLoadCallback = Function(String search);

class HomeScreen extends StatefulWidget {
  
  final void Function() onInit;
  final OnLoadCallback onLoad;

  HomeScreen({
    @required this.onInit,
    @required this.onLoad
    }) ;

  @override
  HomeScreenState createState() {
    return HomeScreenState();
  }
  
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    widget.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Gliphy"),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            child: FormItem(onSearch: (text) {
              widget.onLoad(text);
            }),
            padding: const EdgeInsets.all(10),
          ),
          Expanded(child: LoadedImages())
        ],
      )
    );
  } 
}