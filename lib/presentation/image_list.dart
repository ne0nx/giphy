
import 'package:flutter/material.dart';
import 'package:giphy/containers/app_loading.dart';

import 'package:giphy/presentation/loading_indicator.dart';
import 'package:giphy/presentation/image_item.dart';

import 'package:giphy/models/image.dart' as Models;

typedef OnLoadMoreCallback = Function();

class ImageList extends StatelessWidget {

  final bool isLoading;
  final List<Models.Image> images;
  final OnLoadMoreCallback onLoadMore;
  final ScrollController scrollController = ScrollController();

  ImageList({
    this.isLoading = false,
    this.images = const [], 
    this.onLoadMore}) {
    scrollController.addListener(_scrollListener);
  }
  
  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      _buildLoading(),
      _buildGrid()
    ]);
  }


  void _scrollListener() {
    if (scrollController.offset >= scrollController.position.maxScrollExtent
        && !scrollController.position.outOfRange) {
          onLoadMore();
    }
  }

  Widget _buildLoading() {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 300),
      opacity: isLoading ? 1.0 : 0.0,
      child: Container(
        alignment: FractionalOffset.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildGrid() {
    return AnimatedOpacity(
        duration: Duration(milliseconds: 300),
        opacity: isLoading ? 0.0 : 1.0,
        child: GridView.builder(
          padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 20),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5
          ),
          controller: scrollController,
          itemBuilder: (BuildContext context, int index) {
            return ImageItem(
                image: images[index]
            );
          },
          itemCount: images.length,
        )
    );
  }

}