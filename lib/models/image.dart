
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:giphy/repositories/image_entity.dart';

@immutable
class Image {

  final String url;

  Image({this.url});

  static Image fromEntity(ImageEntity entity) {
    return Image(url: entity.url);
  }

}