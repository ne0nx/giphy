import 'package:giphy/repositories/image_request.dart';
import 'package:giphy/repositories/web_client.dart';
import 'package:giphy/selectors/selectors.dart';
import 'package:giphy/states/search_state.dart';
import 'package:redux/redux.dart';

import 'package:giphy/repositories/images_repository.dart';
import 'package:giphy/repositories/giphy/giphies_repository_flutter.dart';
import 'package:giphy/repositories/giphy/giphy_request_builder.dart';
import 'package:giphy/models/image.dart';
import 'package:giphy/states/app_state.dart';
import 'package:giphy/actions/actions.dart';

List<Middleware<AppState>> createStoreMiddleware([
  ImagesRepository repository = const GiphiesRepositoryFlutter(
    client: const WebClient(),
    builder: GiphyRequestBuilder(
      endpoint: 'https://api.giphy.com/v1/gifs/search',
      apiKey: "4157yOvfKI7AytkHT215ZaoHPWr7ECk7"
    )
  )
]) {
  final loadImages = _createLoadImages(repository);
  final loadMoreImages = _createLoadMoreImages(repository);
  return [
    TypedMiddleware<AppState, LoadImagesAction>(loadImages),
    TypedMiddleware<AppState, LoadMoreImagesAction>(loadMoreImages),
  ];
}

Middleware<AppState> _createLoadImages(ImagesRepository repository) {
  return (Store<AppState> store, action, NextDispatcher next) {
    SearchState search = searchSelector(store.state);
    ImageRequest request = ImageRequest(
        search: search.query,
        page: 0);

    repository.loadImages(request: request)
        .then((images) {
          var list = images.map(Image.fromEntity).toList();
          store.dispatch(ImagesLoadedAction(images: list));
        })
        .catchError((_) =>
          store.dispatch(ImagesNotLoadedAction()));

      next(action);
  };
}

Middleware<AppState> _createLoadMoreImages(ImagesRepository repository) {
  return (Store<AppState> store, action, NextDispatcher next) {
    SearchState search = searchSelector(store.state);
    ImageRequest request = ImageRequest(
        search: search.query,
        page: search.page);

    repository.loadImages(request: request)
        .then((images) {
          var list = images.map(Image.fromEntity).toList();
          store.dispatch(ImagesLoadedMoreAction(images: list));
        })
        .catchError((_) =>
          store.dispatch(ImagesNotLoadedAction()));

      next(action);
  };
}