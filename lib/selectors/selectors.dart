
import 'package:giphy/states/app_state.dart';
import 'package:giphy/models/image.dart';
import 'package:giphy/states/search_state.dart';

List<Image> imagesSelector(AppState state) => state.images;
bool isLoadingSelector(AppState state) => state.isLoading;
SearchState searchSelector(AppState state) => state.search;

String querySelector(SearchState state) => state.query;
int pageSelector(SearchState state) => state.page;