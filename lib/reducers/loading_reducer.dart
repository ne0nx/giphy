
import 'package:redux/redux.dart';
import 'package:giphy/actions/actions.dart';

final loadingReducer = combineReducers<bool>([
  TypedReducer<bool, LoadImagesAction>(_setLoading),
  TypedReducer<bool, LoadMoreImagesAction>(_setLoading),
  TypedReducer<bool, ImagesLoadedAction>(_setLoaded),
  TypedReducer<bool, ImagesLoadedMoreAction>(_setLoaded),
  TypedReducer<bool, ImagesNotLoadedAction>(_setLoaded),
]);

bool _setLoading(bool state, action) {
  return true;
}

bool _setLoaded(bool state, action) {
  return false;
}