
import 'package:redux/redux.dart';
import 'package:giphy/actions/actions.dart';
import 'package:giphy/models/image.dart';

final imagesReducer = combineReducers<List<Image>>([
  TypedReducer<List<Image>, ImagesLoadedAction>(_setLoadedImages),
  TypedReducer<List<Image>, ImagesLoadedMoreAction>(_addLoadedImages),
]);

List<Image> _setLoadedImages(List<Image> images, ImagesLoadedAction action) {
  return action.images;
}

List<Image> _addLoadedImages(List<Image> images, ImagesLoadedMoreAction action) {
  return List.from(images)..addAll(action.images);
}