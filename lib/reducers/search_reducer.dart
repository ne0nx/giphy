

import 'package:giphy/states/search_state.dart';
import 'package:redux/redux.dart';
import 'package:giphy/actions/actions.dart';

final queryReducer = combineReducers<String>([
  TypedReducer<String, LoadImagesAction>(_setQuery),
]);

String _setQuery(String query, LoadImagesAction action) {
  return action.query.isEmpty ? query : action.query;
}

final pageReducer = combineReducers<int>([
  TypedReducer<int, LoadImagesAction>(_dropPage),
  TypedReducer<int, ImagesLoadedAction>(_setPage),
  TypedReducer<int, ImagesLoadedMoreAction>(_setPage),
]);

int _dropPage(int page, action) {
  return 0;
}

int _setPage(int page, action) {
  return page + 1;
}

SearchState searchStateReducer(SearchState state, action) {
  return SearchState(
    query: queryReducer(state.query, action),
    page: pageReducer(state.page, action)
  );
}