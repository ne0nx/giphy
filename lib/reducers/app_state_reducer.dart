
import 'package:giphy/states/app_state.dart';
import 'package:giphy/reducers/loading_reducer.dart';
import 'package:giphy/reducers/images_reducer.dart';
import 'package:giphy/reducers/search_reducer.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    images: imagesReducer(state.images, action), 
    isLoading: loadingReducer(state.isLoading, action),
    search: searchStateReducer(state.search, action)
    );
}