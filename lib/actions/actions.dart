
import 'package:giphy/models/image.dart';

class LoadImagesAction {
  final String query;

  LoadImagesAction({
    this.query = ''
    });
}

class ImagesLoadedAction {
  final List<Image> images;

  ImagesLoadedAction({
    this.images
    });
}

class LoadMoreImagesAction {
  LoadMoreImagesAction();
}

class ImagesLoadedMoreAction {
  final List<Image> images;

  ImagesLoadedMoreAction({
    this.images
    });
}

class ImagesNotLoadedAction { }