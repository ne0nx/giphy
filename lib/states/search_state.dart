
import 'package:meta/meta.dart';

@immutable
class SearchState {
  final String query;
  final int page;

  const SearchState({
    this.query = 'random',
    this.page = 0
  });

  @override
  int get hashCode => query.hashCode ^ page.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchState &&
          runtimeType == other.runtimeType &&
          query == other.query &&
          page == other.page;
}