
import 'package:giphy/states/search_state.dart';
import 'package:meta/meta.dart';
import 'package:giphy/models/image.dart';

@immutable
class AppState {
  final bool isLoading;
  final List<Image> images;
  final SearchState search;

  AppState({
    this.isLoading = false,
    this.images = const [],
    this.search = const SearchState()
    });

  AppState copyWith({
    bool isLoading,
    List<Image> images,
    SearchState searchState
  }) {
    return AppState(
      isLoading: isLoading ?? this.isLoading,
      images: images ?? this.images,
      search: searchState ?? this.search,
    );
  }

  @override
  int get hashCode =>
    isLoading.hashCode ^
    images.hashCode ^ 
    search.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          isLoading == other.isLoading &&
          images == other.images && 
          search == other.search;
}