
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:giphy/states/app_state.dart';
import 'package:giphy/reducers/app_state_reducer.dart';

import 'package:giphy/actions/actions.dart';
import 'package:giphy/middleware/store_middleware.dart';

import 'package:giphy/presentation/home_screen.dart';
import 'package:giphy/theme.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final store = Store<AppState>(
    appReducer,
    initialState: AppState(isLoading: true),
    middleware: createStoreMiddleware()
  );

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store, 
      child: MaterialApp(
        theme: AppTheme.darkTheme,
        routes: {
          "/": (context) {
            return HomeScreen(
              onInit: () {
                StoreProvider.of<AppState>(context)
                  .dispatch(LoadImagesAction());
              }, 
              onLoad: (String search) {
                StoreProvider.of<AppState>(context)
                  .dispatch(LoadImagesAction(query: search));
              }
            );
          }
        },
      ),
    );
  }
}