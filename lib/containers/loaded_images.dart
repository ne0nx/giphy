

import 'package:flutter/material.dart';
import 'package:giphy/actions/actions.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:giphy/states/app_state.dart';
import 'package:giphy/models/image.dart' as Models;
import 'package:giphy/selectors/selectors.dart';

import 'package:giphy/presentation/image_list.dart';

class LoadedImages extends StatelessWidget {
  LoadedImages({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.fromStore(store),
      builder: (context, viewModel) {
        return ImageList(
          isLoading: viewModel.isLoading,
          images: viewModel.images,
          onLoadMore: viewModel.onLoadMore,
        );
      },
    );
  }

}

class _ViewModel {
  final bool isLoading;
  final List<Models.Image> images;
  final OnLoadMoreCallback onLoadMore;

  _ViewModel({
    @required this.isLoading,
    @required this.images, 
    @required this.onLoadMore
    });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      isLoading: isLoadingSelector(store.state),
      onLoadMore: () {
        store.dispatch(LoadMoreImagesAction());
      },
      images: imagesSelector(store.state),
    );
  }
}