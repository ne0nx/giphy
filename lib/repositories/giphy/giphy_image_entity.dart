
class GiphyImageEntity {
  final String url;
  final String width;
  final String height;

  GiphyImageEntity({this.url, this.width, this.height});

  @override
  int get hashCode =>
      url.hashCode ^ width.hashCode ^ height.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GiphyImageEntity &&
          runtimeType == other.runtimeType &&
          url == other.url &&
          width == other.width &&
          height == other.height;

  static GiphyImageEntity fromJson(dynamic json) {
    return GiphyImageEntity(
      url: json['url'] as String,
      width: json['width'] as String,
      height: json['height'] as String
      );
  }
}