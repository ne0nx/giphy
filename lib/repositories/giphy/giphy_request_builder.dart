import 'package:giphy/repositories/giphy/giphy_request.dart';
import 'package:meta/meta.dart';

class GiphyRequestBuilder {

  final String endpoint;
  final String apiKey;

  const GiphyRequestBuilder({ @required this.endpoint, @required this.apiKey });

  String _joinParameters(GiphyRequest request) {
    Map parameters = request.toMap();
    String combinedParameters = parameters.keys
      .map((value) => "$value=${parameters[value]}")
      .join('&');
    return combinedParameters;
  }

  String build({ @required GiphyRequest request }) {
    String combinedParameters = _joinParameters(request);
    return "$endpoint?$combinedParameters&api_key=$apiKey";
  } 

}