
import 'package:giphy/repositories/image_entity.dart';
import 'package:giphy/repositories/giphy/giphy_image_entity.dart';

class GiphyEntity extends ImageEntity {

  final String type;
  final String id;
  final Map<String, GiphyImageEntity> images;

  GiphyEntity({this.type, this.id, this.images}) 
    : super(null);
  
  String get url {
    return images['fixed_width'].url;
  }

  @override
  int get hashCode =>
      type.hashCode ^ id.hashCode ^ images.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GiphyEntity &&
          runtimeType == other.runtimeType &&
          type == other.type &&
          id == other.id &&
          images == other.images;

  static GiphyEntity fromJson(dynamic json) {
    Map<String, dynamic> images = json['images'];
    Map<String, GiphyImageEntity> entities = 
      images.map((key, value) => MapEntry(key, GiphyImageEntity.fromJson(images[key])));

    return GiphyEntity(
      id: json['id'],
      type: json['type'],
      images: entities
      );
  }

}