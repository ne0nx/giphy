import 'package:meta/meta.dart';

class GiphyRequest {
  final String query;
  final String lang;
  final int limit; 
  final int offset;
  final String rating;

  GiphyRequest({
    @required this.query, 
    this.lang = 'en', 
    this.limit, 
    this.offset,
    this.rating = 'G'});

  Map<String, Object> toMap() {
    return {
      'q': query,
      'limit': limit,
      'offset': offset,
      'lang': lang,
      'rating': rating,
    };
  }
}