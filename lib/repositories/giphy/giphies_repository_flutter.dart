
import 'dart:convert';

import 'package:giphy/repositories/giphy/giphy_entity.dart';
import 'package:giphy/repositories/giphy/giphy_request.dart';
import 'package:giphy/repositories/giphy/giphy_request_builder.dart';
import 'package:giphy/repositories/image_entity.dart';
import 'package:giphy/repositories/image_request.dart';
import 'package:giphy/repositories/images_repository.dart';
import 'package:giphy/repositories/web_client.dart';
import 'package:meta/meta.dart';

class GiphiesRepositoryFlutter implements ImagesRepository {
  final WebClient client;
  final GiphyRequestBuilder builder;

  const GiphiesRepositoryFlutter({
    @required this.client,
    @required this.builder
    });

  List<GiphyEntity> _parseEntities(String responceBody) {
    Map<String, Object> json = jsonDecode(responceBody);
    List<dynamic> items = json["data"];

    var entities = items.map(GiphyEntity.fromJson).toList();
    return entities;
  }

  @override
  Future<List<ImageEntity>> loadImages({ ImageRequest request }) async {
    const int loadCount = 26;

    GiphyRequest internalRequest = GiphyRequest(
      query: request.search, 
      offset: request.page * loadCount,
      limit: loadCount
      );
    String url = builder.build(request: internalRequest);

    return await client
      .get(url)
      .then((response) => 
        _parseEntities(response.body)
        );
  }
  
}