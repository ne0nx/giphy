
import 'package:giphy/repositories/image_entity.dart';
import 'package:giphy/repositories/image_request.dart';
import 'package:giphy/repositories/images_repository.dart';
import 'package:giphy/repositories/giphy/giphy_entity.dart';

class MockGiphiesRepositoryFlutter implements ImagesRepository {

  MockGiphiesRepositoryFlutter();

  @override
  Future<List<ImageEntity>> loadImages({ ImageRequest request }) {
    return Future.delayed(Duration(milliseconds: 3000), () => [
      GiphyEntity(),
      GiphyEntity(),
      GiphyEntity()
    ]);
  }
  
}