

abstract class ImageEntity {
  final String url;

  ImageEntity(this.url);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ImageEntity &&
          runtimeType == other.runtimeType &&
          url == other.url;

  @override
  int get hashCode => url.hashCode;
}