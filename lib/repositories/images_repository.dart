
import "./image_entity.dart";
import "./image_request.dart";

abstract class ImagesRepository {

  Future<List<ImageEntity>> loadImages({ ImageRequest request });

}