
import 'package:http/http.dart';

class WebClient {
  const WebClient();

  Future<Response> get(url) async {
    Client client = Client();
    return client.get(url);
  }

}