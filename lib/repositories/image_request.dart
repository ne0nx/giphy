

class ImageRequest {
  final String search;
  final int page;

  ImageRequest({
    this.search, 
    this.page
    });
}